from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from django import forms
from .forms import TodoItemForm, TodoListForm

# Create your views here.


def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    return render(request, "todos/todo_list.html", {"todo_lists": todo_lists})


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_items = todo_list.items.all()
    return render(
        request,
        "todos/todo_list_detail.html",
        {
            "todo_list": todo_list,
            "todo_items": todo_items,
        },
    )


def todo_list_create_view(request):
    class TodoListForm(forms.ModelForm):
        class Meta:
            model = TodoList
            fields = ["name"]

    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            party = form.save()
            return redirect(
                "todos:todo_list_detail", party.id
            )  # Redirect to the list view
    else:
        form = TodoListForm()
    return render(request, "todos/todo_list_create.html", {"form": form})


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    class TodoListForm(forms.ModelForm):
        class Meta:
            model = TodoList
            fields = ["name"]

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect(
                "todos:todo_list_detail", id=id
            )  # Redirect to the detail view
    else:
        form = TodoListForm(instance=todo_list)

    return render(request, "todos/todo_list_update.html", {"form": form})


def delete_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todos:todo_list_list")

    return render(request, "todos/delete_todo_list.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        print(request)
        if form.is_valid():
            item = form.save()
            return redirect("todos:todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    return render(request, "todos/todo_item_create.html", {"form": form})


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            item = form.save()
            return redirect("todos:todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    return render(request, "todos/todo_item_update.html", {"form": form})
