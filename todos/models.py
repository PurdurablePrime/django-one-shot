from django.db import models
from django.utils import timezone

from django.apps import apps

class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(blank=True, null=True)  # Optional due_date
    is_completed = models.BooleanField(
        default=False
    )  # Default is_completed to False
    list = models.ForeignKey(
        "todos.TodoList", related_name="items", on_delete=models.CASCADE
    )  # ForeignKey relationship with TodoList
